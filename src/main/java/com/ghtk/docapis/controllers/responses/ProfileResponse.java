package com.ghtk.docapis.controllers.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileResponse {

  private String name;
  private String code;
  private String description;
}
