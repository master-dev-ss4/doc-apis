package com.ghtk.docapis.controllers;

import com.ghtk.docapis.controllers.requests.CreateProfileRequest;
import com.ghtk.docapis.controllers.requests.UpdateProfileRequest;
import com.ghtk.docapis.controllers.responses.ProfileResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springdoc.core.converters.models.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(
    description = "Profile resources that provides access to available profile data",
    name = "Profile Resource")
@RestController
@RequestMapping("/v1.0/profile")
public class ProfileController {

  @Operation(summary = "Create profile", description = "Create new profile")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "${api.response-codes.ok.desc}"),
        @ApiResponse(
            responseCode = "400",
            description = "${api.response-codes.badRequest.desc}",
            content = {@Content(examples = {@ExampleObject(value = "")})}),
        @ApiResponse(
            responseCode = "404",
            description = "${api.response-codes.notFound.desc}",
            content = {@Content(examples = {@ExampleObject(value = "")})})
      })
  @PostMapping("")
  public ResponseEntity<ProfileResponse> createProfile(@RequestBody CreateProfileRequest request) {
    // TODO
    return ResponseEntity.ok(new ProfileResponse());
  }

  @Operation(summary = "Get profile", description = "Get profile by id")
  @GetMapping("/{profileId}")
  public ResponseEntity<Object> getProfile(@PathVariable(name = "profileId") Long profileId) {
    // TODO
    return ResponseEntity.ok("Get profile success");
  }

  @Operation(summary = "Update profile", description = "Update profile with id")
  @PutMapping("/{profileId}")
  public ResponseEntity<Object> updateProfile(
      @PathVariable(name = "profileId") Long profileId, @RequestBody UpdateProfileRequest request) {
    // TODO
    return ResponseEntity.ok("Update profile success");
  }

  @Operation(summary = "Delete profile", description = "Delete profile with id")
  @DeleteMapping("/{profileId}")
  public ResponseEntity<Object> deleteProfile(@PathVariable(name = "profileId") Long profileId) {
    // TODO
    return ResponseEntity.ok("Delete profile success");
  }

  @Operation(summary = "Search profile", description = "Search profile available")
  @GetMapping("/search")
  public ResponseEntity<Object> searchProfiles(@ParameterObject Pageable pageable) {
    // TODO
    return ResponseEntity.ok("Search profiles success");
  }
}
