package com.ghtk.docapis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocApisApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocApisApplication.class, args);
	}

}
